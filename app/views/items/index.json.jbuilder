json.array!(@items) do |item|
  json.extract! item, :id, :sku, :master_price, :cost_price, :note, :supplier_id
  json.url item_url(item, format: :json)
end
