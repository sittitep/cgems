json.array!(@materials) do |material|
  json.extract! material, :id, :name, :weight, :amount, :price, :note, :item_id, :supplier_id
  json.url material_url(material, format: :json)
end
