json.array!(@suppliers) do |supplier|
  json.extract! supplier, :id, :name, :supplyable_id, :supplyable_type
  json.url supplier_url(supplier, format: :json)
end
