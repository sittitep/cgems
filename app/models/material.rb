class Material < ActiveRecord::Base
  belongs_to :item
  belongs_to :supplier

  after_commit :update_item_price

  def update_item_price
    return false unless self.item.present?
    
    item = self.item
    item.master_price = item.materials.sum(:price) * 1.5
    item.cost_price = item.materials.sum(:price)
    item.save
  end
end
