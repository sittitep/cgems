class Item < ActiveRecord::Base
  belongs_to :supplier
  has_many :materials
  accepts_nested_attributes_for :materials, allow_destroy: true
end
