class Supplier < ActiveRecord::Base
  belongs_to :supplyable, polymorphic: true
end
