class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :sku
      t.decimal :master_price, precision: 8, scale: 2
      t.decimal :cost_price, precision: 8, scale: 2
      t.text :note
      t.references :supplier, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
