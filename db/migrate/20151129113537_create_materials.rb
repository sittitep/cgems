class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :name
      t.decimal :weight, precision: 8, scale: 2
      t.integer :amount
      t.decimal :price, precision: 8, scale: 2
      t.text :note
      t.references :item, index: true, foreign_key: true
      t.references :supplier, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
