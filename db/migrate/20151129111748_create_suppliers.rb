class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name
      t.references :supplyable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
