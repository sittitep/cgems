# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
sup1 = Supplier.create(name: "John")
sup2 = Supplier.create(name: "Anna")

diamond = Material.create(name: "diamond", weight: 0.3, amount: 1, price: 15000, supplier: sup1)
gold = Material.create(name: "gold", weight: 1, amount: 1, price: 15000, supplier: sup2)

item = Item.new(
    sku: 000001,
    master_price: 45000,
    cost_price: 60000,
    on_hands: 1 
  )
item.materials = [diamond,gold]
item.save