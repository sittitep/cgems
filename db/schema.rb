# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151129121315) do

  create_table "items", force: :cascade do |t|
    t.string   "sku",          limit: 255
    t.decimal  "master_price",               precision: 8, scale: 2
    t.decimal  "cost_price",                 precision: 8, scale: 2
    t.text     "note",         limit: 65535
    t.integer  "supplier_id",  limit: 4
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "state",        limit: 255
  end

  add_index "items", ["supplier_id"], name: "index_items_on_supplier_id", using: :btree

  create_table "materials", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.decimal  "weight",                    precision: 8, scale: 2
    t.integer  "amount",      limit: 4
    t.decimal  "price",                     precision: 8, scale: 2
    t.text     "note",        limit: 65535
    t.integer  "item_id",     limit: 4
    t.integer  "supplier_id", limit: 4
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
  end

  add_index "materials", ["item_id"], name: "index_materials_on_item_id", using: :btree
  add_index "materials", ["supplier_id"], name: "index_materials_on_supplier_id", using: :btree

  create_table "suppliers", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "supplyable_id",   limit: 4
    t.string   "supplyable_type", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "suppliers", ["supplyable_type", "supplyable_id"], name: "index_suppliers_on_supplyable_type_and_supplyable_id", using: :btree

  add_foreign_key "items", "suppliers"
  add_foreign_key "materials", "items"
  add_foreign_key "materials", "suppliers"
end
